using Crefisa.NC.Comum.Config;
using Crefisa.NC.Infraestrutura.Dados;
using NFCLogAPI.Entities;

namespace NFCLogAPI.Repository {
    public class LogRepository : RepositorioBase<LogEntity> {
        public LogRepository(string connection = "ConnectionCUC") : base(new DbConnectionConfigElement { ConnectionString = connection }) {
        }
    }
}

