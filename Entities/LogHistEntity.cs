using Crefisa.NC.Comum.Atributos;
using Crefisa.NC.Comum.Interfaces;
using System;

namespace NFCLogAPI.Entities {
[EntidadeDB(ProcedureInclusao  = "PC_INSLOGHIST")]
	
    public class LogHistEntity: IEntidadeDB { // Contém o histórico de cada mês dos logs de alterações das tabelas
        [ColunaDB("COD_LOGHIST", true)] public int codLoghist { get; set; }
        [ColunaDB("VLR_AAAAMM")] public string vlrAaaamm { get; set; }
        [ColunaDB("COD_LOG")] public int codLog { get; set; }
        [ColunaDB("NOM_TABELA")] public string nomTabela { get; set; }
        [ColunaDB("COD_REGISTRO")] public int codRegistro { get; set; }
        [ColunaDB("DTH_INSERT")] public DateTime dthInsert { get; set; }
        [ColunaDB("DTH_UPDATE")] public DateTime dthUpdate { get; set; }
        [ColunaDB("DTH_DELETE")] public DateTime dthDelete { get; set; }
        [ColunaDB("COD_USERINSERT")] public int codUserinsert { get; set; }
        [ColunaDB("COD_USERUPDATE")] public int codUserupdate { get; set; }
        [ColunaDB("COD_USERDELETE")] public int codUserdelete { get; set; }
        [ColunaDB("VLR_ORIGINAL")] public object vlrOriginal { get; set; }
        [ColunaDB("VLR_ATUAL")] public object vlrAtual { get; set; }
        [ColunaDB("VLR_DELETE")] public object vlrDelete { get; set; }
        [ColunaDB("NOM_PROCESSOORIGEM")] public string nomProcessoorigem { get; set; }
        [ColunaDB("NOM_SISTEMAORIGEM")] public string nomSistemaorigem { get; set; }
    }
}
