using Crefisa.NC.Comum.Atributos;
using Crefisa.NC.Comum.Interfaces;
using System;

namespace NFCLogAPI.Entities {
[EntidadeDB(ProcedureAlteracao = "PC_UPDLOG", ProcedureInclusao  = "PC_INSLOG")]
	
    public class LogEntity: IEntidadeDB { // Contém o log de alterações das tabelas do mês atual, a cada mês os dados são movidos para a tabela de hitórico de logs
        [ColunaDB("COD_LOG", true)] public int codLog { get; set; }
        [ColunaDB("NOM_TABELA")] public string nomTabela { get; set; }
        [ColunaDB("COD_REGISTRO")] public int codRegistro { get; set; }
        [ColunaDB("DTH_INSERT")] public DateTime dthInsert { get; set; }
        [ColunaDB("DTH_UPDATE")] public DateTime dthUpdate { get; set; }
        [ColunaDB("DTH_DELETE")] public DateTime dthDelete { get; set; }
        [ColunaDB("COD_USERINSERT")] public int codUserinsert { get; set; }
        [ColunaDB("COD_USERUPDATE")] public int codUserupdate { get; set; }
        [ColunaDB("COD_USERDELETE")] public int codUserdelete { get; set; }
        [ColunaDB("VLR_ORIGINAL")] public object vlrOriginal { get; set; }
        [ColunaDB("VLR_ATUAL")] public object vlrAtual { get; set; }
        [ColunaDB("VLR_DELETE")] public object vlrDelete { get; set; }
        [ColunaDB("NOM_PROCESSOORIGEM")] public string nomProcessoorigem { get; set; }
        [ColunaDB("NOM_SISTEMAORIGEM")] public string nomSistemaorigem { get; set; }
    }
}
