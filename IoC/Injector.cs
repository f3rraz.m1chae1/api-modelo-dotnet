﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using NFCLogAPI.AutoMapper;
using NFCLogAPI.Interfaces;
using Crefisa.NC.Comum.Interfaces;
using NFCLogAPI.AppService;
using NFCLogAPI.Entities;
using NFCLogAPI.Repository;

namespace NFCLogAPI.IoC {
    /// <summary>
    /// Class to management of services injection.
    /// </summary>
    public static class Injector {

        /// <summary>
        /// Register services used in the application
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        public static void RegisterServices(IServiceCollection services, IConfiguration configuration) {
            services.AddCors();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddDistributedMemoryCache();
            services.AddAutoMapper();

            services.AddScoped<ILogAppService, LogAppService>();
            services.AddScoped<IRepositorio<LogEntity>, LogRepository>();
            services.AddScoped<ILogHistAppService, LogHistAppService>();
            services.AddScoped<IRepositorio<LogHistEntity>, LogHistRepository>();

            var mappingConfig = new MapperConfiguration(mc => {
                mc.AddProfile(new NFCLogAPIProfile());
            });
            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.AddSwaggerGen(c => {
                c.SwaggerDoc("v1", new Info {
                    Version = "v1",
                    Title = "NFCLogAPI",
                    Description = "API de Log do NFC ASP.NET Core Web API",
                    TermsOfService = "None",
                    Contact = new Contact {
                        Name = "Sistemas Internos",
                        Email = "sistemasinternos@crefisa.com.br",
                        Url = "https://twitter.com/spboyer"
                    },
                    License = new License {
                        Name = "No Defined",
                        Url = "https://example.com/license"
                    }
                });
            });

        }

    }
}

