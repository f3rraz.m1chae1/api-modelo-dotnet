using NFCLogAPI.Interfaces;
using NFCLogAPI.Repository;
using NFCLogAPI.Entities;
using NFCLogAPI.DTO;
using AutoMapper;
using System.Linq;
using System.Collections.Generic;
using Crefisa.NC.Comum.Interfaces;

namespace NFCLogAPI.AppService {
    public class LogAppService : ILogAppService {
        private Microsoft.Extensions.Logging.ILogger _LoggerFactory;
        private IMapper mapper;

        private IRepositorio<LogEntity> logRepository;

        public LogAppService(Microsoft.Extensions.Logging.ILogger<LogAppService> loggerFactory, IMapper mapper_, IRepositorio<LogEntity> logRepository) {
            _LoggerFactory = loggerFactory;
            mapper = mapper_;
            this.logRepository = logRepository;
        }


        public int Inserir(LogDTO dto) { 
            var entity = mapper.Map<LogEntity>(dto); 
            logRepository.Inserir(entity); 
            var novoDto = mapper.Map<LogDTO>(entity); 
            return novoDto.codLog; 
        } 


        public void Alterar(LogDTO dto) { 
            var entity = mapper.Map<LogEntity>(dto); 
            logRepository.Atualizar(entity); 
        } 



    }
}

