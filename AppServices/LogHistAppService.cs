using NFCLogAPI.Interfaces;
using NFCLogAPI.Repository;
using NFCLogAPI.Entities;
using NFCLogAPI.DTO;
using AutoMapper;
using System.Linq;
using System.Collections.Generic;
using Crefisa.NC.Comum.Interfaces;

namespace NFCLogAPI.AppService {
    public class LogHistAppService : ILogHistAppService {
        private Microsoft.Extensions.Logging.ILogger _LoggerFactory;
        private IMapper mapper;

        private IRepositorio<LogHistEntity> logHistRepository;

        public LogHistAppService(Microsoft.Extensions.Logging.ILogger<LogHistAppService> loggerFactory, IMapper mapper_, IRepositorio<LogHistEntity> logHistRepository) {
            _LoggerFactory = loggerFactory;
            mapper = mapper_;
            this.logHistRepository = logHistRepository;
        }


        public int Inserir(LogHistDTO dto) { 
            var entity = mapper.Map<LogHistEntity>(dto); 
            logHistRepository.Inserir(entity); 
            var novoDto = mapper.Map<LogHistDTO>(entity); 
            return novoDto.codLoghist; 
        } 




    }
}

