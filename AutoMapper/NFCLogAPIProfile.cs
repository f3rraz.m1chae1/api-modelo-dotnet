﻿using AutoMapper;
using NFCLogAPI.DTO;
using NFCLogAPI.Entities;

namespace NFCLogAPI.AutoMapper {

    /// <summary>
    /// Classe de profile das classes que serão utilizadas na aplicação via AutoMapper.
    /// </summary>
    public class NFCLogAPIProfile : Profile {

        /// <summary>
        /// Construtor da classe NFCLogAPIProfile
        /// </summary>
        public NFCLogAPIProfile() {
            CreateMap<LogDTO, LogEntity>();
            CreateMap<LogEntity, LogDTO>();
            CreateMap<LogHistDTO, LogHistEntity>();
            CreateMap<LogHistEntity, LogHistDTO>();
        }

    }
}
