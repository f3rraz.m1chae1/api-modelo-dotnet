using NFCLogAPI.DTO;
using System.Collections.Generic;

namespace NFCLogAPI.Interfaces {
    public interface ILogAppService {
        
        int Inserir(LogDTO logDto);
		void Alterar(LogDTO logDto);
		
		
    }
}
