using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using AutoMapper;
using NFCLogAPI.DTO;
using NFCLogAPI.Interfaces;
using Crefisa.NC.Comum.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace NFCLogAPI.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class LogController : ControllerBase {

        private readonly ILogAppService appService;
        private readonly ILogger logger;
        private IMapper mapper_;

        public LogController(ILogAppService appS, ILogger<LogController> logg, IMapper mapper) {
            appService = appS;
            logger = logg;
            mapper_ = mapper;
        }

        /// <summary>
        /// Alterar log
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, AllowAnonymous, Route("v1/alterar/")] 
        public async Task<ActionResult> Alterar(LogDTO model) { 
            try { 
                appService.Alterar(model); 
                return Ok(); 
            } 
            catch (Exception ex) { 
                logger.LogError(ex, "[LogController.LogUpdate] - Erro ao alterar o registro." + ex.Message + " | StackTrace = " + ex.StackTrace, null); 
                return BadRequest(); 
            } 
        } 

        /// <summary>
        /// Inserir log
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut, AllowAnonymous, Route("v1/inserir/")] 
        public async Task<ActionResult> Inserir(LogDTO model) { 
            var retorno = new ResponseDTO<int>(); 
            try { 
                retorno.Data = appService.Inserir(model); 
                retorno.Status = retorno.Data != 0; 
                retorno.Message = retorno.Status ? "OK" : "Registro não inserido"; 
                retorno.ExceptionMessage = null; 
                return Ok(retorno); 
            } 
            catch (Exception ex) { 
                logger.LogError(ex, "[LogController.LogInsert] - Erro ao inserir o registro." + ex.Message + " | StackTrace = " + ex.StackTrace, null); 
                retorno.Status = false; 
                retorno.ExceptionMessage = ex.Message; 
                return BadRequest(retorno); 
            } 
        } 



    }
}
