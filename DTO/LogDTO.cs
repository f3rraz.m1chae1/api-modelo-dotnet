using System;
using System.Collections.Generic;

namespace NFCLogAPI.DTO {
    public class LogDTO { // Contém o log de alterações das tabelas do mês atual, a cada mês os dados são movidos para a tabela de hitórico de logs
        public int codLog { get; set; }
        public string nomTabela { get; set; }
        public int codRegistro { get; set; }
        public DateTime dthInsert { get; set; }
        public DateTime dthUpdate { get; set; }
        public DateTime dthDelete { get; set; }
        public int codUserinsert { get; set; }
        public int codUserupdate { get; set; }
        public int codUserdelete { get; set; }
        public object vlrOriginal { get; set; }
        public object vlrAtual { get; set; }
        public object vlrDelete { get; set; }
        public string nomProcessoorigem { get; set; }
        public string nomSistemaorigem { get; set; }
	}
}
